from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import cgi

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from database_setup import Base, Restaurant

# connect to the database and keep alive
engine = create_engine('sqlite:///restaurantmenu.db')
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()

class webserverHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        try:
            if self.path.endswith("/restaurants") or self.path.endswith("/"):
                self.send_response(200)
                self.send_header('Content-type', 'text/html')
                self.end_headers()

                restaurants = session.query(Restaurant).all()

                output = "<html>"
                output += "<h3>Restaurants</h3>"
                output += "<a href='/restaurants/new'><button>Create new restaurant</button></a>"
                for restaurant in restaurants:
                    output += "<div>"
                    output += "<p>%d. %s</p>" % (restaurant.id, restaurant.name)
                    output += "<a href='/restaurants/%d/edit'>Edit </a>" % restaurant.id
                    output += "<a href='/restaurants/%d/delete'>Delete</a>" % restaurant.id
                    output += "</div>"
                output += "</html>"

                self.wfile.write(output)
                return

            if self.path.endswith("/restaurants/new"):
                self.send_response(200)
                self.send_header('Content-type', 'text/html')
                self.end_headers()

                output = "<html>"
                output += "<h3>Create new restaurant</h3>"
                output += "<form method='POST' enctype='multipart/form-data' action='/restaurants/new'>"
                output += "<input name='restaurantname' placeholder='Name of restaurant' type='text'>"
                output += "<input type='submit' value='Create'>"
                output += "</form>"
                output += "</html>"

                self.wfile.write(output)
                return

            if self.path.endswith("/edit"):
                # Get the id from url
                restaurantID = self.path.split("/")[2]
                query_restaurant = session.query(Restaurant).filter_by(id=restaurantID).one()

                if query_restaurant:
                    self.send_response(200)
                    self.send_header('Content-type', 'text/html')
                    self.end_headers()

                    output = "<html>"
                    output += "<h4>Update %s's Name</h4>" % query_restaurant.name
                    output += "<form method='POST' enctype='multipart/form-data' action='/restaurants/%s/edit'>"
                    output += "<input name='newRestaurantName' placeholder='%s' type='text'>" % (restaurantID, query_restaurant.name)
                    output += "<input type='submit' value='Rename'>"
                    output += "</form>"
                    output += "</html>"

                    self.wfile.write(output)
                    return

            if self.path.endswith("/delete"):
                # Get the id from url
                restaurantID = self.path.split("/")[2]
                query_restaurant = session.query(Restaurant).filter_by(id=restaurantID).one()

                if query_restaurant:
                    self.send_response(200)
                    self.send_header('Content-type', 'text/html')
                    self.end_headers()

                    output = "<html>"
                    output += "<h4>Update %s's Name</h4>" % query_restaurant.name
                    output += "<form method='POST' enctype='multipart/form-data' action='/restaurants/%s/delete'>" % restaurantID
                    output += "<p>Are you sure you want to delete %s?</p>" % query_restaurant.name
                    output += "<input type='submit' value='Yes, delete'>"
                    output += "</form>"
                    output += "</html>"

                    self.wfile.write(output)
                    return

        except IOError:
            self.send_error(404, 'File not found %s' % self.path)


    def do_POST(self):
        try:
            if self.path.endswith("/restaurants/new"):
                ctype, pdict = cgi.parse_header(self.headers.getheader('content-type'))
                if ctype == 'multipart/form-data':
                    fields=cgi.parse_multipart(self.rfile, pdict)
                messagecontent = fields.get('restaurantname')

                # Create new Restaurant object
                newRestaurant = Restaurant(name=messagecontent[0])
                session.add(newRestaurant)
                session.commit()

                self.send_response(301)
                self.send_header('Content-type', 'text/html')
                self.send_header('Location', '/restaurants')
                self.end_headers()

            if self.path.endswith("/edit"):
                ctype, pdict = cgi.parse_header(
                    self.headers.getheader('content-type'))
                if ctype == 'multipart/form-data':
                    fields = cgi.parse_multipart(self.rfile, pdict)
                    messagecontent = fields.get('newRestaurantName')

                    restaurantID = self.path.split("/")[2]

                    query_restaurant = session.query(Restaurant).filter_by(
                        id=restaurantID).one()

                    if query_restaurant != []:
                        # Update the db
                        query_restaurant.name = messagecontent[0]
                        session.add(query_restaurant)
                        session.commit()

                        self.send_response(301)
                        self.send_header('Content-type', 'text/html')
                        self.send_header('Location', '/restaurants')
                        self.end_headers()

            if self.path.endswith("/delete"):
                ctype, pdict = cgi.parse_header(
                    self.headers.getheader('content-type'))
                if ctype == 'multipart/form-data':
                    fields = cgi.parse_multipart(self.rfile, pdict)

                    restaurantID = self.path.split("/")[2]

                    query_restaurant = session.query(Restaurant).filter_by(
                        id=restaurantID).one()

                    if query_restaurant != []:
                        # Update the db
                        session.delete(query_restaurant)
                        session.commit()

                        self.send_response(301)
                        self.send_header('Content-type', 'text/html')
                        self.send_header('Location', '/restaurants')
                        self.end_headers()

        except:
            pass


def main():
    try:
        port = 8080
        server = HTTPServer(('', port), webserverHandler)
        print "Web server running on port %s" % port
        server.serve_forever()

    except KeyboardInterrupt:
        print " stopping web server..."
        server.socket.close()

if __name__ == '__main__':
    main()
