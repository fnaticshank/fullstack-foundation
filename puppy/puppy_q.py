from sqlalchemy import create_engine
from sqlalchemy import func
from sqlalchemy.orm import sessionmaker

from puppy_setup import Base, Shelter, Puppy
from random import randint

import datetime
import random

engine = create_engine('sqlite:///puppyshelter.db')
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()


# helper function
def echo(f):
    def wrap():
        print "\n-----------------------------\n Running function %s \
                \n-----------------------------\n" % (f.func_name)
        return f()
    return wrap


# queries

@echo
def query_one():
    """Query all the puppies an return the results in ascending alphabetical
    order.
    """
    result = session.query(Puppy.name).order_by(Puppy.name.asc()).all()
    for pup in result:
        print pup.name


@echo
def query_two():
    """Query all of the puppies that are less than 6 months old organized by
    the youngest first.
    """
    today = datetime.date.today()
    result = session.query(Puppy.name, Puppy.dateOfBirth).order_by(Puppy.dateOfBirth.desc()).all()

    for pup in result:
        delta = today - pup[1]
        months = delta.days/30
        if months < 6:
            print "{name}, {months}".format(name=pup[0], months=pup[1])


@echo
def query_three():
    """Query all puppies by ascending weight."""
    result = session.query(Puppy.name, Puppy.weight).order_by(Puppy.weight.asc()).all()

    for pup in result:
        print("{name}, {weight}".format(name=pup[0], weight=pup[1]))


@echo
def query_four():
    """Query all puppies grouped by the shelter in which they are staying."""
    result = session.query(Shelter, func.count(Puppy.id)).join(Puppy).group_by(Shelter.id).all()
    for shelter in result:
        print shelter[0].id, shelter[0].name, shelter[1]


query_one()
query_two()
query_three()
query_four()

