# CRUD operations

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from database_setup import Base, Restaurant, MenuItem

engine = create_engine('sqlite:///restaurantmenu.db')
Base.metadata.bind = engine
DBSession = sessionmaker(bind = engine)
session = DBSession()

# create
myRestaurant = Restaurant(name = 'Pizza plaza')
session.add(myRestaurant)
session.commit()

cheesepizza = MenuItem(name="Cheese Pizza",
                       description = "Made with all natural ingredients and \
                        fresh mozzarella",
                       course="Entree", price="$8.99",
                       restaurant=myRestaurant)
session.add(cheesepizza)
session.commit()


# read
# we use the session.query method to retrieve results from db
# some methods are first(), one(), all(), filter_by().
firstResult = session.query(Restaurant).first()
firstResult.name

items = session.query(MenuItem).all()
for item in items:
    print item.name


# update
# in order to update an entry, we execute the following commands:
# - Find entry
# - Update value(s)
# - Add to session
# - Commit session

# finding the entry
veggieBurgers = session.query(MenuItem).filter_by(name = 'Veggie Burger')
for burger in veggieBurgers:
    print burger.id
    print burger.name
    print burger.restaurant.name
    print '\n'

# update the entry
UrbanVeggieBurger = session.query(MenuItem).filter_by(id=8).one()
UrbanVeggieBurger.price = '$2.99'

# add and commit
session.add(UrbanVeggieBurger)
session.commit()


# delete
# to delete an entry we `session.delete(entry)` method
spinach = session.query(MenuItem).filter_by(name='Spinach Ice Cream').one()
session.delete(spinach)
session.commit()
